/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pts2;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 *
 * @author qfernand
 */
public class FXMLDocumentController implements Initializable {
    private ArrayList<String> humanite;
    @FXML
    private Label label;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("GROS QUENTIN!");
    }
    private void detruireHumnanite(ActionEvent event) {

        humanite = new ArrayList<>();
        humanite.add("Bouffons");
        humanite.add("Bouffonnes");
        humanite.remove(0);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("Hello World !");
    }    
    
}
